
# 联系方式
- 手机：13132717627
- Email：aklivecai@gmail.com 

---

# 个人信息

 - 陈波/男/1988 
 - 大专/柳州职业技术学院软件技术
 - 工作年限：5年
 - 期望职位：PHP程序开发

---

# 工作经历

## 深圳市具人网络科技有限公司 （ 2012年9月 ~ 2016年7月 ）

### 管理系统 ( http://oa.775m.com/ )
我在此项目主要负责人,项目主要是分为 客户管理,仓库管理,以及 `` 家具专搜 ``平台上的信息管理 
> 项目基于`` Yii2 ``框架,自豪的技术细节是 自定义批量导入( 拖拽形式) 

### 家具专搜 ( http://www.775m.com/ )
我在此项目主要是负责后台数据库设计,功能的实现,以及前端效果实现
> 项目是基于`` Yii2 ``框架实现的,用到 `` Memcached `` 来进行数据的缓存,`` Mongodb `` 数据库实现不同类型的信息之间差异

### 其他项目
- 早先基于`` Yii1 ``开发的过功能板块: 订单管理,库存管理,销售管理,采购管理,生产管理,计件工资管理,审批管理
- 同时结合`` AngularJS ``开发移动网页版


## 深圳市时代万网科技有限公司 （ 2010年11月 ~ 2012年9月 ）
主要工作是见模板网站，偏前端版面实现为主,以下列出一些还能正常访问,有维护过的
### 建站 
- B2B
  - http://www.9juren.com/
  - http://www.rubcn.com/
- 企业站    
    - http://www.sz-fad.com.cn/
    - http://m.sz-fad.com.cn/
    - http://www.sz-fad.com/
    - http://www.bocarni.com/
    - http://m.gdzcl.com/

---

# 技能清单

以下均为我熟练使用的技能

- Web开发：PHP
- Web框架：Yii/ThinkPHP
- 前端框架：Bootstrap/AngularJS/Jquery
- 前端工具：Gulp
- 数据库相关：MySQL/SQLite/Mongodb/SqlServer
- 版本管理、自动化部署工具：Svn/Git/Composer
- 单元测试：PHPUnit 
- 涉及过: CMS/B2C/B2B/CRM/OA

---